package com.example.minhha_19._bai1;

public class cau_1 {
    public static void main(String[] args) {
        String stringLiteral1 = "hello";
        String stringObject1 = new String("hello");

        System.out.println("Dung " + "'=='" + ": " + (stringLiteral1 == stringObject1));
        System.out.println("Dung " + "'equal'" + ": " + stringObject1.equals(stringLiteral1));
    }
}
