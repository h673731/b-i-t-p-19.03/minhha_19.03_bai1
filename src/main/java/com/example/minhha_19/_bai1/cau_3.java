package com.example.minhha_19._bai1;

import java.util.Scanner;

public class cau_3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Nhap chuoi: ");
        String chuoi = scanner.nextLine();

        System.out.println("Nhap ki tu: ");
        String kyTu = scanner.next();

        int viTri = chuoi.indexOf(kyTu);

        if (viTri >= 0) {
            System.out.println("Co");
        } else {
            System.out.println("Khong");
        }
    }
}
