package com.example.minhha_19._bai1;

public class cau_2 {
    public static void main(String[] args) {
        String sayHello = "Hello My Friends";
        System.out.println("Ban dau: " + sayHello);

//      Loại bỏ tất cả các dấu cách và in String mới ra màn hình
        System.out.println("Bo space: " + sayHello.replaceAll(" ", ""));

//      Chuyển tất cả về kiểu chữ thường và in String mới ra màn hình
        System.out.println("Chu thuong: " + sayHello.toLowerCase());

//      Bỏ đoạn ký tự "Hello " và in String mới ra màn hình (Output là "My Friends")
        System.out.println("Bo Hello: " + sayHello.replaceAll("Hello", ""));

//      Bỏ 2  đoạn ký tự "Hello " và "ends" và in String mới ra màn hình (Output là "My Fri")
        System.out.println("Bo Hello va ends: " + sayHello.replaceAll("Hello", "").replaceAll("ends", ""));

//      Thay đoạn ký tự "Friends" thành "Bro" và in String mới ra màn hình (Output là "Hello My Bro")
        System.out.println("Thay Friends = Bro: " + sayHello.replaceAll("Friends", "Bro"));

//      Tìm vị trí của chữ "My" trong String và in ra màn hình
        System.out.println("Vi tri My: " + sayHello.indexOf("My"));
    }
}
