package com.example.minhha_19._bai1;

import java.util.Scanner;

public class cau_5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Nhap chuoi: ");
        String chuoi = scanner.nextLine();

        if (chuoi.length() <= 20 && !chuoi.contains(" ")
                && Character.isUpperCase(chuoi.charAt(0))
                && Character.isDigit(chuoi.charAt(chuoi.length() - 1))) {
            System.out.println("Hop le!");
        } else {
            System.out.println("Khong hop le!");
        }
    }
}
