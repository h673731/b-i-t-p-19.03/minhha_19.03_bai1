package com.example.minhha_19._bai1;

import java.util.Scanner;

public class cau_4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Nhap chuoi: ");
        String chuoi = scanner.nextLine();
        System.out.print("Nhap ky tu: ");
        String chuoiKyTu = scanner.nextLine();

        for (int i = 0; i < chuoi.length(); i++) {
            char kyTu = chuoi.charAt(i);
            if (chuoiKyTu.contains(String.valueOf(kyTu))) {
                System.out.println("Ky tu '" + kyTu + "' o vi tri " + (i + 1));
            }
        }
    }
}
